<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr>
        <th>
            Book id
        </th>
        <th>
            Book title
        </th>
        <th>
            Book Author
        </th>
        <th>
            Action
        </th>
    </tr>
    <c:forEach items="${book}" var="book">
        <tr>
            <td><c:out value="${book.id}"/></td>
            <td><c:out value="${book.title}"/></td>
            <td><c:out value="${book.author}"/></td>
            <td><form action="/book/confirmBookDelete" method="post">
                <input type="hidden" name="id" value="<c:out value="${book.id}"/>" >
                <%--<input type="hidden" name="title" value="<c:out value="${book.title}"/>">--%>
                <%--<input type="hidden" name="author" value="<c:out value="${book.author}"/>">--%>
                <input type="submit" value="delete"> </form>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>