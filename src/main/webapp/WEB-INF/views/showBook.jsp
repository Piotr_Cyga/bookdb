<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<table>
    <tr>
        <th>
            Book id
        </th>
        <th>
            Book title
        </th>
        <th>
            Book Author
        </th>
    </tr>
    <tr>
        <td>
            ${id}
        </td>
        <td>
            ${title}
        </td>
        <td>
            ${author}
        </td>
    </tr>
</table>
</body>
</html>