<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
</head>
<body>
<h1>
Really delete?
</h1>
<table>
    <tr>
        <th>
            Book id
        </th>
        <th>
            Book title
        </th>
        <th>
            Book Author
        </th>
    </tr>
    <tr>
        <td>
            ${id}
        </td>
        <td>
            ${title}
        </td>
        <td>
            ${author}
        </td>
    </tr>
</table>
<form action="/book/delete" method="post">
    <input type="hidden" name="id" value="${id}"/>
    <input id=submitButton type="submit" value="Pewnie, wywalaj"/>
</form>
<button>
<a href="list">albo jednak nie!</a>
</button>
</body>
</html>