package application;

import entities.User;
import org.springframework.context.annotation.Bean;


public class UserService {
    UserDAOImpl userDAO = new UserDAOImpl();

@Bean
    public User loginUser(String name, String password) {
        User user = userDAO.getUser(name);
        if (user.getPassword() == password) {
            return user;
        }
        return null;
    }
}
