package application;

import entities.Book;

import java.util.List;

public interface BookDAO {
    public Book create(Book book);
    public Book get(int id);
    public List<Book> listBooks();
    public boolean remove(int id);
}
