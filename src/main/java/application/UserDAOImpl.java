package application;

import entities.Book;
import entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class UserDAOImpl implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public User getUser(String name) {
        Session currentSession = currentSession();
        return currentSession.get(User.class, name);
    }
    @Transactional
    @Override
    public boolean addUser(User user) {
        currentSession().save(user);
        return true;
    }
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
}
