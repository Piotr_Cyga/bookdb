package application;

import entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BookCrudController {
    @Autowired
    BookDAO bookDAO;

    @RequestMapping(value = "/book/view/{id:[^[0-9]*[1-9][0-9]*$]+}", method = RequestMethod.GET)
    public ModelAndView showBook(@PathVariable("id") int id) {
        Book book = new Book();
        book = bookDAO.get(id);
        ModelAndView modelAndView = new ModelAndView("showBook");
        modelAndView.addObject("title", book.getTitle());
        modelAndView.addObject("author", book.getAuthor());
        modelAndView.addObject("id", book.getId());
        return modelAndView;
    }

    @RequestMapping(value = "/book/add/{title:[A-Za-z0-9 ]+}/{author:[A-Za-z0-9 ]+}", method = RequestMethod.GET)
    public ModelAndView addBook(@PathVariable("title") String title, @PathVariable("author") String author) {
        Book book = new Book(title, author);
        bookDAO.create(book);
        ModelAndView modelAndView = new ModelAndView("showBook");
        modelAndView.addObject("title", book.getTitle());
        modelAndView.addObject("author", book.getAuthor());
        modelAndView.addObject("id", book.getId());

        return modelAndView;
    }
    @RequestMapping(value = "/book/list", method = RequestMethod.GET)
    public ModelAndView listBooks() {
        ModelAndView modelAndView = new ModelAndView("list");
        modelAndView.addObject("book", bookDAO.listBooks());

        return modelAndView;
    }
    @RequestMapping(value = "/book/confirmBookDelete", method = RequestMethod.POST)
    public ModelAndView removeBook(@RequestParam("id") int id) {
        Book book = bookDAO.get(id);
        ModelAndView modelAndView = new ModelAndView("confirmBookDelete");
        modelAndView.addObject("title", book.getTitle());
        modelAndView.addObject("author", book.getAuthor());
        modelAndView.addObject("id", book.getId());
        return modelAndView;
    }
    @RequestMapping(value = "/book/delete", method = RequestMethod.POST)
    public ModelAndView confirmRemoveBook(@RequestParam("id") int id) {
        bookDAO.remove(id);
        ModelAndView modelAndView = new ModelAndView("list");
        modelAndView.addObject("book", bookDAO.listBooks());
        return modelAndView;
    }
}
