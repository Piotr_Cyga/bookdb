package application;

import entities.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Repository
public class BookDAOImpl implements BookDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    @Transactional
    public Book create(Book book) {
        Session currentSession = currentSession();
        currentSession.saveOrUpdate(book);
        return book;
    }
    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
    @Override
    @Transactional
    public Book get(int id) {
        Session currentSession = currentSession();
        return currentSession.get(Book.class, id);
            }

    @Override
    @Transactional
    public List<Book> listBooks() {
        return sessionFactory.getCurrentSession().createQuery("from Book").getResultList();
    }

    @Override
    @Transactional
    public boolean remove(int id) {
        if (sessionFactory.getCurrentSession().get(Book.class, id) != null) {
            Book book = sessionFactory.getCurrentSession().get(Book.class, id);
            sessionFactory.getCurrentSession().remove(book);
            return true;
        }
        return false;
    }
}
