package application;

import entities.User;

public interface UserDao {
    User getUser(String name);
    boolean addUser(User user);
}
